		const app = Vue.createApp({
			data() {
				return {
					addUrl:'192.168.128.88',
					//基金编号
					fS_code: '005962',//'010017', //'001182',
					//基金名称
					fS_name: '',
					//现费率
					fund_Rate: '',
					//基金的历史净值
					/*x-日期
					  y-基金净值
					  equityReturn-净值回报 
					  unitMoney-每份派送金*/
					Data_netWorthTrend: [],
					dates: [],
					shortcuts: [],
					//累计收益
					income_arr:[],
					dealType:null,
					dealOptions:[{value:'0',label:'默认买卖记录'},
								 {value:'1',label:'按平均净值买卖'},
								 {value:'2',label:'All in最大收益'},
								 {value:'3',label:'All in最大亏损'},
								 {value:'5',label:'每周定投'},
								 {value:'6',label:'每两周定投'},
								 {value:'4',label:'每月定投'},
								 {value:'7',label:'每日'},
								 
					],
					avgYrange:[],
					btop:true,
					dayNum:null,
					//买卖记录，最后的方式，是追加到每日净值上
					dealsRed: [
						// {x: Date.parse(new Date(2021, 2, 18)),y: null,buySell: null,sumMoney: 1000},
						// {x: Date.parse(new Date(2021, 2, 24)),y: null,buySell: null,sumMoney: 100}
						{
							x: Date.parse(new Date(2020, 9 - 1, 1)),
							y: null,
							buySell: 288.91,
							sumMoney: 288.9
						},
						{
							x: Date.parse(new Date(2021, 3 - 1, 3)),
							y: null,
							buySell: -144.46,
							sumMoney: null
						},
						{
							x: Date.parse(new Date(2021, 3 - 1, 5)),
							y: null,
							buySell: -134,
							sumMoney: null
						}
					],
					//当前买入记录
					inputBuy: {
						x: null,
						y: null,
						buy: 0,
						ignore:false,
						sumMoney: 0
					},
					//当前卖出记录
					inputSell: {
						x: null,
						y: null,
						sell: 0,
						sumMoney: 0
					},
					
					disabledDate(time) {
						let bchkDate = (this.dates && this.dates.length == 2);
						let begDate = null;
						let endDate = null;
						if (bchkDate) {
							begDate = this.dates[0];
							endDate = this.dates[1];
							console.log('begDate,endDate,time', begDate, endDate, time);
							return (time.getTime() >= begDate.getTime() && time.getTime() <= endDate.getTime());
						} else return false;
					}
				}
			},
			computed: {
				/*持仓信息*/
				holdInfo() {
					//持有份额
					let amount = 0;
					//持仓成本
					let sumMoney = 0;
					//持仓成本价
					let price = 0;
					//收益
					let income = 0;
					this.dealsRed.forEach((red) => {
						amount = this.ForDight(amount + (red.buySell ? red.buySell : 0), 2);
						if (red.sumMoney > 0) {
							//总持仓成本
							sumMoney = this.ForDight(sumMoney + red.sumMoney, 2);
							//持仓成本价
							price = sumMoney && amount ? this.ForDight(sumMoney / amount, 4) : null;
						}
					})
					income = this.ForDight((this.lastFundData.y - price) * amount, 2);

					return {
						amount: amount,
						price: price,
						sumMoney: this.ForDight(price * amount, 2),
						income: income,
						income_pt: this.ForDight(income / sumMoney * 100, 2)
					}
				},

				lastFundData() { //最后一个交易日的净值信息
					if (this.Data_netWorthTrend && this.Data_netWorthTrend.length > 0) {
						return this.Data_netWorthTrend[this.Data_netWorthTrend.length - 1]
					} else {
						return null;
					}
				},
				begFundData() { //第一个交易日的净值信息
					if (this.Data_netWorthTrend && this.Data_netWorthTrend.length > 0) {
						return this.Data_netWorthTrend[0]
					} else {
						return null;
					}
				},
				top_dealsRed(){
					if(this.dealsRed && this.dealsRed.length > 0){
						let topNum = this.btop ? 5:this.dealsRed.length;
						return this.dealsRed.reverse().slice(0,topNum);	
					}else return null;
				}
			},
			mounted: function() {
				this.shortcuts = getShortCuts(null, null);
			},
			methods: {
				changeTop(){
				  //console.log('btop',this.btop);
				  //this.btop	
				},
				
				//保留小数
				ForDight: function(Dight, How) {
					Dight = Math.round(Dight * Math.pow(10, How)) / Math.pow(10, How);
					return Dight;
				},
				onSelDealType:function(){
					// {value:'5',label:'每周定投'},
					// {value:'6',label:'每两周定投'},
					// {value:'4',label:'每月定投'},
					// {value:'7',label:'每日'},
					//console.log('交易记录变更:::',this.dealType,this.dayNum)
					let itype = this.dealType;
					if(itype == 1){
						//按平均净值买卖
						//调用makeDealArr.js的函数获取交易记录
						this.dealsRed = avgDealRecd(this.Data_netWorthTrend,this.dates,1);
						// this.dealsRed  = [];
					}
					else if(itype == 2){
						//All in最大收益
						this.dealsRed = maxDealRecd(this.Data_netWorthTrend,this.dates,this.fund_Rate);
					}
					else if(itype == 3){
						//All in最大亏损
						this.dealsRed = minDealRecd(this.Data_netWorthTrend,this.dates,this.fund_Rate);
					}
					else if(itype == 4 && this.dayNum){
						//每月定投
						console.log('每月1号定投',this.dayNum);
						this.dealsRed = monthDayDealRecd(this.Data_netWorthTrend,this.dates,0,this.dayNum);
					}
					else if(itype == 5 && this.dayNum){
						//每周定投
						console.log('每周1定投', this.dayNum);		
						this.dealsRed = monthDayDealRecd(this.Data_netWorthTrend,this.dates,1,this.dayNum);
					}
					else if(itype == 6 && this.dayNum){
						//每两周定投
						console.log('每两周1定投',this.dayNum);
						this.dealsRed = monthDayDealRecd(this.Data_netWorthTrend,this.dates,2,this.dayNum);
					}
					else if(itype == 7 ){
						//每两周定投
						console.log('每日定投',this.dayNum);
						this.dealsRed = monthDayDealRecd(this.Data_netWorthTrend,this.dates,3,null);
					}
					else if(itype == 0){
						//默认交易记录
					   this.dealsRed = [
						   {
								x: Date.parse(new Date(2020, 9 - 1, 1)),
								y: null,
								buySell: 288.91,
								sumMoney: 288.9
							},
							{
								x: Date.parse(new Date(2021, 3 - 1, 3)),
								y: null,
								buySell: -144.46,
								sumMoney: null
							},
							{
								x: Date.parse(new Date(2021, 3 - 1, 5)),
								y: null,
								buySell: -134,
								sumMoney: null
							}]
					}
					this.updateDealRedY();
					this.changeDate();
					
				},
				updatBuy: function() {
					this.inputBuy.y = null;
					this.inputBuy.buy = null;
					if (this.inputBuy && this.inputBuy.x && this.Data_netWorthTrend) {
						let fndData =
							this.Data_netWorthTrend.find(data => {
								if (data.x == this.inputBuy.x.getTime())
									return data;
							})

						if (fndData) {
							//更新净值
							if (fndData.y)
								this.inputBuy.y = fndData.y;
							//更新份额
							if (this.inputBuy.sumMoney){
								
								this.inputBuy.buy = this.inputBuy.sumMoney * 
												(1 - (this.inputBuy.ignore ? 0: this.fund_Rate) / 100) /
												 this.inputBuy.y;
							}
						}

					}
				},
				updateSell: function() {
					this.inputSell.y = null;
					this.inputSell.sumMoney = null;
					if (this.inputSell.x && this.Data_netWorthTrend) {
						let fndData =
							this.Data_netWorthTrend.find(data => {
								if (data.x == this.inputSell.x.getTime())
									return data;
							})

						if (fndData) {
							//更新净值
							if (fndData.y)
								this.inputSell.y = fndData.y;
							//更新份额
							if (this.inputSell.sell)
								this.inputSell.sumMoney = this.inputSell.y * this.inputSell.sell * (1 - this
									.fund_Rate / 100);
						}

					}
				},
				inputBuySell: function(itype, input) {
					//买入 itype=1
					//卖出 itype=2
					console.log('input',input);
					let buySell = null;
					if (itype == 1)
						buySell = input.buy
					else if (itype == 2)
						buySell = input.sell;

					let dealRed = {
						x: input.x.getTime(),
						y: input.y,
						buySell: buySell,
						sumMoney: input.sumMoney
					};
					let item =
						this.dealsRed.find(item => {
							if (dealRed.x == item.x)
								return item;
						});

					if (item == null) {
						this.dealsRed.push(dealRed);
						this.changeDate();
					}
				},
				
				del_buySellRed:function(red,index){
				   //删除交易记录
				   //更新图表				   
				   if(index > -1 && index < this.dealsRed.length){
					   this.dealsRed.splice(index,1);
					   this.changeDate();
				   }

				},
				
				//根据交易记录的日期，获取当天的交易净值，以数组的形式返回
				//更新交易记录上的净值
				updateDealRedY: function() {

					let idx0 = 0,
						idx1 = 0;
					for (; idx0 < this.Data_netWorthTrend.length; idx0++) {
						if (idx1 < this.dealsRed.length && this.Data_netWorthTrend[idx0].x == this.dealsRed[idx1].x) {
							this.dealsRed[idx1].y = this.Data_netWorthTrend[idx0].y;
							if (this.dealsRed[idx1].sumMoney && this.dealsRed[idx1].buySell == null) {
								this.dealsRed[idx1].buySell = this.ForDight(this.dealsRed[idx1].sumMoney * 
								     (1 -this.fund_Rate / 100) / this.dealsRed[idx1].y, 2);
							} else if (this.dealsRed[idx1].buySell) {
								this.dealsRed[idx1].sumMoney = this.ForDight(this.dealsRed[idx1].buySell *
								     this.dealsRed[idx1].y / (1 - this.fund_Rate / 100), 2);
							}

							idx1++;
						} else if (idx1 >= this.dealsRed.length) {
							break;
						}
					}

					console.log('updateDealRedY', this.dealsRed);
				},
				getDateStr: function(dateStr) {
					if (dateStr && dateStr != '') {
						let tmpDate = new Date(dateStr);
						let text = tmpDate.getFullYear() + '-' + (tmpDate.getMonth() + 1) + '-' + tmpDate
							.getDate()
						return text;
					} else return null;
				},
				getFundData: async function() {
					this.fS_name = '';
					this.fund_Rate = '';
					this.Data_netWorthTrend = [];
					this.dates = [];
					this.loadEChart(this.fS_code, this.fS_name, this.Data_netWorthTrend, this.dates);

					if (this.fS_code !== '') {
						try {
							let dataInfo = await requestFundData(this.fS_code,this.addUrl);
							alert('请求成功');
							// ElMessage('查询成功');
							const jsCode = dataInfo.data;
							// console.log('执行文本代码');
							eval(jsCode);

							this.fS_name = fS_name;
							this.fund_Rate = fund_Rate;
							this.Data_netWorthTrend = Data_netWorthTrend;

							//更新买卖的净值金额
							this.updateDealRedY();

							//console.log('获取基金的的信息', this.fS_name, this.fund_Rate, this.Data_netWorthTrend);
							this.shortcuts = getShortCuts(this.lastFundData.x, this.begFundData.x);
							//默认定义最近一年
							if (this.shortcuts && this.shortcuts.length > 1) {
								this.dates = this.shortcuts[this.shortcuts.length - 2].value;
							}
							this.loadEChart(this.fS_code, this.fS_name, this.Data_netWorthTrend, this.dates);

						} catch (e) {
							alert('请求失败');
						}
					}
				},
				changeDate: function() {
					this.loadEChart(this.fS_code, this.fS_name, this.Data_netWorthTrend, this.dates);
				},

				loadEChart: function(fSn, fName, fData, dates) {
					//fSn,fName,fData
					//基金编号，基金名称，基金的历史净值
					// 基于准备好的dom，初始化echarts实例
					let myChart = echarts.init(document.getElementById('main'));
					let bchkDate = (dates && dates.length == 2);
					let begDate = bchkDate ? dates[0]:null;
					let endDate = bchkDate ? dates[1]:null;
					
					//当前显示的净值
					let curData = fData.filter((item) => {
						if (bchkDate && item.x >= begDate && item.x <= endDate) {
							//限制日期范围
							return true;
						} else if (!bchkDate) {
							//不限制日期范围
							return true
						} else return false;
					});
					
					//获取累计收益
					let incomeData = this.getMoneyArr(fData, this.dealsRed).filter((item) => {
						if (bchkDate && item.x >= begDate && item.x <= endDate) {
							//限制日期范围
							return true;
						} else if (!bchkDate) {
							//不限制日期范围
							return true
						} else return false;
					});
					//更新累计收益
					this.income_arr = incomeData;

					//当前的markPoint集合		
					let markPtArr = [];
					let buyCol = 'rgba(252, 85, 49, 0.9)';
					let sellCol= 'rgba(3, 228, 123, 0.9)';
					let incomeCol0 = 'rgba(222, 7, 7, 0.9)';
					let incomeCol1 = 'rgba(219, 219, 219, 0.9)';
					this.dealsRed.forEach((item) => {
						let col = "";
						let y = null;
						if (item.buySell > 0) {
							col = buyCol;
						} else if (item.buySell < 0) {
							col = sellCol;
						}

						if (col != "" && curData && curData.length > 0) {
							markPtArr.push({
								xAxis: item.x,
								yAxis: item.y,
								itemStyle: {
									color: col
								}
							});
						}
					});
					//console.log('markPtArr', markPtArr);

					// 指定图表的配置项和数据
					let option = {
						legend: {
							data: ['累计净值', '累计盈亏']
						},

						tooltip: {
							trigger: 'axis',
							backgroundColor:'rgba(50,50,50,0.7)',
							formatter: (params) => {
								// console.log('params:::',params);
								let item = params[0].data;
								
								let str = '日期：' + params[0].axisValueLabel + '<br/>' +
									params[0].seriesName + "：" + item["y"];

								let curRed =
									this.dealsRed.find(dealRed => {
										if (item["x"] == dealRed.x)
											return dealRed;
									});

								let buyInfo = "";
								let sellInfo = "";
								if (curRed && curRed["buySell"]) {
									if (curRed["buySell"] > 0)
										buyInfo = '<br/><span style="color: '+ buyCol   +'">买入份额：' + this.ForDight( curRed["buySell"] ,2) + "</span>";
									else if (curRed["buySell"] < 0)
									   sellInfo = '<br/><span style="color: '+ sellCol  +'">卖出份额：' + this.ForDight( curRed["buySell"] ,2) + "</span>";
								}
								
								let str1 = "";
								if(params.length > 1){
								   let item1 = params[1].data;
								   str1 = '<br/><span style="color: '
								    +(item1["sumMoney"] >= 0 ? incomeCol0:incomeCol1)
								    +'">'+ params[1].seriesName+'：'+item1["sumMoney"]+'</span>'
									+'<br/><span style="color: '
									+(item1["curMoney"] >= 0 ? incomeCol0:incomeCol1)
									+'">持有收益：'+item1["curMoney"]
									+'<br/>持有份额：'+item1["curAmount"]
									;
								}
							
								str = str + buyInfo + sellInfo+ str1;
								return str;
							}
						},
						xAxis: {
							type: 'time',
							splitLine: {
								show: false
							},
							scale: true,
						},
						yAxis: [{ //第一个Y轴
								position: 'left',
								name: '累计净值',
								type: 'value',
								axisLabel: {
									formatter: '{value}元/份'
								},
								// scale: true,
								boundaryGap: ['0%', '0%'],
								splitLine: {
									show: false
								}
							},
							{ //第二个Y轴
								position: 'right',
								name: '累计盈亏',
								type: 'value',
								axisLabel: {
									formatter: '{value}元'
								},
								// scale: true,
								boundaryGap: ['0%', '0%'],
								splitLine: {
									show: false
								}
							}
						],
						dataset: [
							// 这里指定了维度名的顺序，从而可以利用默认的维度到坐标轴的映射。
							// 如果不指定 dimensions，也可以通过指定 series.encode 完成映射，参见后文。
							// dimensions: ['x', 'y'],
							{source: curData},
							{source: incomeData},
						],
						series: [
							{
								datasetIndex: 0,
								name: '累计净值',
								type: 'line',
								yAxisIndex: '0',
								showSymbol: false,
								hoverAnimation: false,
								markPoint: {
									symbol: "pin", //标注类型
									symbolSize: 20, //标记大小
									symbolOffset: [0, 0],
									data: markPtArr
								},
								label:{
								    normal:{
								        show:true,
								        position:'top'
								    }
								},
								encode: {
									x: 'x',
									y: 'y'
								}
							},
							{
								datasetIndex: 1,
								name: '累计盈亏',
								type: 'line',
								yAxisIndex: '1',
								showSymbol: false,
								hoverAnimation: false,
								label:{
								    normal:{
								        show:true,
								        position:'top'
								    }
								},
								encode: {
									x: 'x',
									y: 'sumMoney'
								}
								// markPoint: {
								// 	symbol: "pin", //标注类型
								// 	symbolSize: 20, //标记大小
								// 	symbolOffset: [0, 0],
								// 	data: markPtArr
								// }
							},

						]
					};

					// 使用刚指定的配置项和数据显示图表。
					myChart.setOption(option);
				},

				getMoneyArr: function(dataArr, dealArr) {
					//累计收益集合，x,y (日期，累计收益)
					let moneyArr = [];
					if (dealArr && dealArr.length > 0) {
						let begIdx = dataArr.findIndex(item => {
							return item.x == dealArr[0].x
						});

						// console.log('getmoneyArr:::,dataArr[0],dealArr[0]',dataArr,dealArr);
						// console.log('getmoneyArr:::begIdx',begIdx);
						if (begIdx >= 0) {
							// console.log('getmoneyArr:::begIdx', begIdx);
							let tmpPrice = 0,
								tmpAmount = 0,
								i = 0,
								sumMoney = 0;
							dataArr.slice(begIdx).forEach(item => {
								//每天的收益
								if (i < dealArr.length && item.x == dealArr[i].x) {
									//当前的均价，当前的份额
									//更新当前总买入的总成本 = 买入前的净值*买入前的份额 + 当日买入的净值*当日买入的份额
									//更新均价
									let deal = dealArr[i];
									if (deal.buySell > 0) {
										// let tmpMoney  = tmpAmount * tmpPrice + deal.y * deal.buySell ;
										let tmpMoney = tmpAmount * tmpPrice + deal.sumMoney;
										tmpPrice = tmpMoney / (tmpAmount + deal.buySell);
									} else if (deal.buySell < 0) {
										sumMoney = this.ForDight(sumMoney + Math.abs(deal.buySell) * (item
											.y - tmpPrice), 2);
									}
									//更新持有份额
									tmpAmount = this.ForDight( tmpAmount + deal.buySell ,2);

									//if(i < dealArr.length)
									i = i + 1;
								}

								//每天持有收益 = 当天份额 × (当天净值-当天均价) 
								//一旦卖出，转换成固定收益
								//累计收益 = 卖出的收益=卖出份额*（卖出净值-当天均价）+ 当天持有收益
								moneyArr.push({
									x: item.x,
									//持有收益
									curMoney: this.ForDight(tmpAmount * (item.y - tmpPrice), 2),
									//持有份额
									curAmount:tmpAmount,
									//累计收益
									sumMoney: this.ForDight(sumMoney + tmpAmount * (item.y -
										tmpPrice), 2)
								});
							});

						}

					}

					return moneyArr;
				}
			}
		});

		let vm = app.use(ElementPlus).mount("#appId");




		//返回Obj数组中，以某个值作为最大
		function getObjMaxValue(arrObj, proName) {
			let maxVal = null;
			let arr = [];
			arrObj.forEach((item) => {
				arr.push(item[proName])
			})

			if (arr.length > 0) {
				maxVal = Math.max(...arr);
			}
			return maxVal;
		}

		//使用axios
		function requestFundData(fundCode,addUrl) {
			//let Url = 'http://127.0.0.1:9898/fund';
			let Url = 'http://'+addUrl+':9898/fund';
			if (Url && Url !== '') {
				return new Promise((resolve, reject) => {
					axios.get(Url, {
							params: {
								'fundCode': fundCode
							}
						})
						.then(response => {
							if (response.status == 200) {
								// console.log('请求成功');
								resolve(response);
							} else {
								reject(new Error('请求失败'))
							}
						})
						.catch(error => {
							console.log('请求失败', error);
							reject(error)
						})
				})
			}
		}


		//定义内部函数
		function getStratEnd(tmpDate, offsetDays) {
			const end = new Date(tmpDate);
			const start = new Date(tmpDate);
			start.setTime(start.getTime() - 3600 * 1000 * 24 * offsetDays);
			return [start, end];
		}


		function getShortCuts(curDate, begDate) {
			let lstDate = Date.now();
			if (curDate && curDate !== '') {
				lstDate = curDate;
			}
			let d0 = new Date(2021, 0, 1);

			let dateShortCuts = [{
					text: '近1月',
					value: getStratEnd(lstDate, 30),
				}, {
					text: '近3月',
					value: getStratEnd(lstDate, 90),
				}, {
					text: '近6月',
					value: getStratEnd(lstDate, 180),
				}, {
					text: '近1年',
					value: getStratEnd(lstDate, 365),
				}, {
					text: '近3年',
					value: getStratEnd(lstDate, 365 * 3),
				}, {
					text: '今年来',
					value: [d0, lstDate]
				}, {
					text: '成立至今',
					value: [begDate, lstDate]
				}

			];

			return dateShortCuts;
		}