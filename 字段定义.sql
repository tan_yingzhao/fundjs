
---投递环节1
一、特快（一）国内特快 n11_exp_in
 n11_exp_in_ref
 n11_exp_in_comput
 n11_exp_in_cost
 
（二）国际特快 n1_1exp_out
 n11_exp_out_ref
 n11_exp_out_comput
 n11_exp_out_cost
 二、快包（一）国内小包（快递包裹） n12_smp_in
	n12_smp_in_ref
	n12_smp_in_comput
	n12_smp_in_cost

（二）国际小包	n12_smp_out
	n12_smp_out_ref
	n12_smp_out_comput
	n12_smp_out_cost

三、个性化业务（其他） n13_person（一）全城通
	n13_person_city_dlv_ref
	n13_person_city_dlv_comput
	n13_person_city_dlv_cost

（二）约投挂号
	n13_person_ord_dlv_ref
	n13_person_ord_dlv_comput
	n13_person_ord_dlv_cost
四、普邮 n14_com（一）条码平信 n14_com_cd_letter
  n14_com_cd_letter_ref
  n14_com_cd_letter_comput
  n14_com_cd_letter_cost

（二）条码报刊 n14_com_cd_paper
  n14_com_cd_paper_ref
  n14_com_cd_paper_comput
  n14_com_cd_paper_cost
  
（三）普通包裹 n14_com_pack
   n14_com_pack_ref
   n14_com_pack_comput
   n14_com_pack_cost

（四）挂号挂刷 n14_com_reg_mail
	n14_com_reg_mail_ref
	n14_com_reg_mail_comput
	n14_com_reg_mail_cost

（五）其他 n14_com_oth
	n14_com_oth_ref
	n14_com_oth_comput
	n14_com_oth_cost
	五、特揽投业务 n15_spec（一）投递	平安车险一程(元) 	n15_spec_pacx1_cost
	平安车险二程(元) 	n15_spec_pacx2_cost
	车牌投递(元)	 		n15_spec_car_num_cost
	电信卡妥投(元)   	n15_spec_dx_card_cost 
	电信卡实名认证(元) 	n15_spec_realname_cost 
	平安人寿(元)	 		n15_spec_pars_cost 
	抵解押业务(元)   	n15_spec_releas_cost 
	车企通业务(元)   	n15_spec_car_comp_cost
	质押解质押业务(元) 	n15_spec_pledge_cost
	移动专票(元)			n15_spec_mobile_cost
	好车主(元)			n15_spec_car_owner_cost
	汽配项目(元)   		n15_spec_car_parts_cost
（二）营销	牌托销售(元)    		n15_spec_car_holder_cost
 


---揽收（含营销）环节2
一、国内特快	 n21_exp
（一）国内特快【大宗客户】n21_exp_in_cust
  n21_exp_in_cust_ref
  n21_exp_in_cust_comput
  n21_exp_in_cust_cost

（二）国内特快【散件】n21_exp_in_single
  n21_exp_in_single_ref
  n21_exp_in_single_comput
  n21_exp_in_single_cost

二、国际特快（国际EMS）n22_ems
（一）国际特快【大宗客户】 n22_ems_out_cust
	n22_ems_out_cust_ref
	n22_ems_out_cust_comput
	n22_ems_out_cust_cost
	
（二）国际特快【散件】n22_ems_out_single
   n22_ems_out_single_ref
   n22_ems_out_single_comput
   n22_ems_out_single_cost

三、国内小包（快递包裹） n23_smp_in
（一）国内小包【大宗客户】n23_smp_in_cust
	n23_smp_in_cust_ref
	n23_smp_in_cust_comput
	n23_smp_in_cust_cost

（二）国内小包【散件】n23_smp_single
	n23_smp_single_ref
	n23_smp_single_comput
	n23_smp_single_cost

四、国际小包	n24_smp_out
（一）国际小包【大宗客户】n24_smp_out_cust
	n24_smp_out_cust_ref
	n24_smp_out_cust_comput
	n24_smp_out_cust_cost

（二）国际小包【散件】n24_smp_out_single
	n24_smp_out_single_ref
	n24_smp_out_single_comput
	n24_smp_out_single_cost

五、国际E邮宝 n25_eyb_out
（一）国际E邮宝
	n25_eyb_out_ref
	n25_eyb_out_comput
	n25_eyb_out_cost

六、国际非邮 n26_out_npost
（一）国际非邮
	n26_out_npost_ref
	n26_out_npost_comput
	n26_out_npost_cost

七、物流业务 n27_mflow
（一）物流业务
	n27_mflow_ref
	n27_mflow_comput
	n27_mflow_cost

八、其他 n28_oth
  其他结算业务收入/元
  n28_oth_ref
  n28_oth_comput
  n28_oth_cost
   

--内部处理3 n3
一、特快	  n31_exp_in
（一）国内特快			
	n31_exp_in_ref
	n31_exp_in_comput
	n31_exp_in_cost

（二）国际特快	
	n31_exp_out_ref
	n31_exp_out_comput
	n31_exp_out_cost	
				
二、快包		n32
（一）国内小包（快递包裹）		n32_smp_in	
	n32_smp_in_ref
	n32_smp_in_comput
	n32_smp_in_cost

（二）国际小包	n32_smp_out
	n32_smp_out_ref
	n32_smp_out_comput
	n32_smp_out_cost

三、个性化业务（其他）	n33_person
（一）全城通			
	n33_person_city_dlv_ref
	n33_person_city_dlv_comput
	n33_person_city_dlv_cost

（二）约投挂号
	n33_person_ord_dlv_ref
	n33_person_ord_dlv_comput
	n33_person_ord_dlv_cost
	
四、国际E邮宝	n34_eyb_out
（一）国际E邮宝
	n34_eyb_out_ref
	n34_eyb_out_comput
	n34_eyb_out_cost	

五、国际非邮	n35_out_npost
（一）国际非邮	
	n35_out_npost_ref
	n35_out_npost_comput
	n35_out_npost_cost

六、普邮
（一）条码平信	
	n36_com_cd_letter_ref
	n36_com_cd_letter_comput
	n36_com_cd_letter_cost		
		
（二）条码报刊	
  n36_com_cd_paper_ref
  n36_com_cd_paper_comput
  n36_com_cd_paper_cost

（三）普通包裹		
	n36_com_pack_ref
	n36_com_pack_comput
	n36_com_pack_cost

（四）挂号挂刷	
	n36_com_reg_mail_ref
	n36_com_reg_mail_comput
	n36_com_reg_mail_cost
 
 
 
---运输4 n4_transport_line1
一干运输（省际）	
   n41_transport_line1_ref
   n41_transport_line1_comput
   n41_transport_line1_cost
		
二干运输（省内）			
   n42_transport_line2_ref
   n42_transport_line2_comput
   n42_transport_line2_cost

市趟运输（市内）
   n43_transport_city_ref
   n43_transport_city_comput
   n43_transport_city_cost


---协同5 n5_bus
一、金融业务		n51_bus_bank
（一）余额		
	n51_bus_bank_save_ref
	n51_bus_bank_save_comput
	n51_bus_bank_save_cost
	
（二）代理保险	n51_bus_bank_dlbx
	n51_bus_bank_dlbx_ref
	n51_bus_bank_dlbx_comput
	n51_bus_bank_dlbx_cost
		
（三）理财		n51_bus_bank_lc
	n51_bus_bank_lc_ref
	n51_bus_bank_lc_comput
	n51_bus_bank_lc_cost

二、邮务业务		n52_bus_post
（一） 函件		n52_bus_post_hj	
	n52_bus_post_hj_ref
	n52_bus_post_hj_comput
	n52_bus_post_hj_cost

（二） 集邮			
	n52_bus_post_jy_ref
	n52_bus_post_jy_comput
	n52_bus_post_jy_cost

（三）报刊			
	n52_bus_post_paper_ref
	n52_bus_post_paper_comput
	n52_bus_post_paper_cost

（四）新媒体		
	n52_bus_post_media_ref
	n52_bus_post_media_comput
	n52_bus_post_media_cost
	
协同合计

