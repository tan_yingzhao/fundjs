function getMaxDataIdx(dataArr) {
	//最大值
	let tmpIndex = 0;
	if (dataArr && dataArr.length > 0) {
		dataArr.forEach((item, index) => {
			if (item.y > dataArr[tmpIndex].y)
				tmpIndex = index
		})
	}
	return tmpIndex;
}

function getMinDataIdx(dataArr) {
	//最小值
	let tmpIndex = 0;
	if (dataArr && dataArr.length > 0) {
		dataArr.forEach((item, index) => {
			if (item.y < dataArr[tmpIndex].y)
				tmpIndex = index
		})
	}
	return tmpIndex;
}

function getDataArr(dataArr, begEndDates) {
	//返回时间段内的净值数组
	let tmpArr = dataArr.filter(item => {
		return (item.x >= begEndDates[0] && item.x <= begEndDates[1])
	})
	return tmpArr;
}

function getAvgY(dataArr) {
	//区间的平均值 
	let avgY = null,		sumY = 0;
	if (dataArr && dataArr.length > 0) {
		sumY = dataArr.reduce((total, item) => {
			return total + item.y;
		});
		avgY = sumY / dataArr.length;
	}
	return avgY;
}

function avgDealRecd(dataArr, begEndDates, itype) {
	//下跌区间低吸
    //itype	1,通过 (持仓净值+新低)/2 中位数，类似每次大跌翻倍加仓，需要较多资金
	//itype 2,通过斜率K<0(下跌)，K>0(上涨)
	//出现第二个新低净值时（首次破新低），判断是否出现持续的下跌区间——
	// |-K| = minY的偏移量 / idx的偏移量，|-k|=相邻两个minY的偏移量 / idx的偏移量
	// 如果|-K|由大到小，|-k|趋于缓和，证明大跌趋于缓和，小区间跌幅趋于缓和；

	let initAmount = 0, initPrice = 0, initMoney = 10000;
	let curAmount = initAmount,		curPrice = initPrice , curMoney = initMoney;
	let yMinMaxAvg = [], lstMinMaxAvg = null , begMinMax = null ,down_pt = null ;
	let rang_K = [], rang_k = [], tmp_K = null , tmp_k = null ;
	let tmpArr = getDataArr(dataArr, begEndDates);
	
	tmpArr.forEach((item, index) => {
		//获取当前的净值数据信息
		let curMinMaxAvg = null;
		if (!lstMinMaxAvg) {
			 curMinMaxAvg = {
				idx: index,
				curY: item.y,
				minY: item.y,
				maxY: item.y,
				avgY: item.y
			};
			
		}else if(lstMinMaxAvg){
				curMinMaxAvg = {
				idx: index,
				curY: item.y,
				minY: Math.min(item.y,lstMinMaxAvg.minY) ,
				maxY: Math.max(item.y,lstMinMaxAvg.maxY),
				avgY: vm.$options.methods.ForDight( (lstMinMaxAvg.avgY * index + item.y) / (index + 1) ,4)
			};
		}
		
		//最低净值变更,破新低
		//if(itype == 1 && curMinMaxAvg && lstMinMaxAvg && curMinMaxAvg.minY < lstMinMaxAvg.minY){
		  if(itype == 1 && curMinMaxAvg?.minY < lstMinMaxAvg?.minY){
		   //(持仓成本+破新低)/2，进行翻倍加仓
			  if(curPrice == 0){
				  //相对于上一个最低净值，下跌幅度,首次判断还是通过平均净值
				  down_pt   = (1- curMinMaxAvg.minY / lstMinMaxAvg.avgY);	
			  }
			  else if(curPrice > 0){
				  //相对于当前的持仓成本，下跌幅度
				  down_pt   = (1- curMinMaxAvg.minY / curPrice);	
			  }
			  else down_pt = null;
			  
			  if(down_pt >= 0.02){
				 //将持仓成本控制到 (curPrice+minY)/2的中位数 0.25~0.5之间
				 let avgPrice = null ,buySell = null;
				 if(curPrice == 0){
					//用每天定投的均值，作为持仓成本，100作为持仓份额
					buySell  = (curMinMaxAvg.avgY * 100 - item.y  * 100) / (curMinMaxAvg.avgY - item.y);	  
				 }else{
					//用当前（最低净值+持仓成本）/2作为中位数，每次买入将翻倍
					avgPrice = Math.min( (item.y + curPrice) / 2 , item.y * 11/10 , item.y * 12/10 ) ;  
					buySell  = vm.$options.methods.ForDight( (curPrice * curAmount - avgPrice * curAmount) / (avgPrice - item.y) ,2) ;	 
				 }
				 curPrice  =  (curPrice * curAmount +  item.y * buySell) / (curAmount + buySell)  ;
				 curAmount = curAmount + buySell; 
				 
				 yMinMaxAvg.push({
				 		x:item.x,
				 		y:item.y,
				 		buySell:buySell,
				 		sumMoney:vm.$options.methods.ForDight( item.y*buySell ,2)
				 }); 
		     }
		  
		   }
		   else if(itype == 2 && curMinMaxAvg && lstMinMaxAvg ){
			//itype 2,通过斜率K<0(下跌)，K>0(上涨)
			//出现第二个新低净值时（首次破新低），判断是否出现持续的下跌区间——
			// |-K| = minY的偏移量 / idx的偏移量，|-k|=相邻两个minY的偏移量 / idx的偏移量
			// 如果|-K|由大到小，|-k|趋于缓和，证明大跌趋于缓和，小区间跌幅趋于缓和；			   
			   
				if(curMinMaxAvg.minY < lstMinMaxAvg.minY){
					//破新低，之后区间均值
					!begMinMax ? begMinMax = lstMinMaxAvg:null;
					tmp_K = (curMinMaxAvg.minY - begMinMax.minY) / (curMinMaxAvg.idx - begMinMax.idx) ;
					tmp_k = (curMinMaxAvg.minY - lstMinMaxAvg.minY) / (curMinMaxAvg.idx - lstMinMaxAvg.idx) ;	
				}else if(curMinMaxAvg.maxY > lstMinMaxAvg.maxY){
					//创新高
						
				}
			
		   }
		   
	   
		lstMinMaxAvg = curMinMaxAvg;
	});
	
	return yMinMaxAvg;
}




function maxDealRecd(dataArr, begEndDates, rate) {
	//获取最大收益的交易记录，单买，单卖
	let tmpArr = getDataArr(dataArr, begEndDates);
	let maxDeals = [];
	let maxOffset = [];
	tmpArr.forEach((item, index) => {
		let maxIdx = null;
		if (index + 1 < tmpArr.length) {
			maxIdx = getMaxDataIdx(tmpArr.slice(index)) + index;
		} else {
			//当前为最后一个净值
			maxIdx = index;
		}
		maxOffset.push({
			offset: tmpArr[maxIdx].y - item.y,
			idx: index,
			idx1: maxIdx
		});
	})

	console.log('净值数组计数', tmpArr.length, '最大收益数组计数', maxOffset.length);
	let topOffset =
		maxOffset.sort((item0, item1) => {
			return item1.offset - item0.offset;
		});
	console.log('排序后的交易结果', topOffset);

	if (topOffset && topOffset.length > 0) {
		let tmp = topOffset[0];
		let idx = tmp.idx;
		let idx1 = tmp.idx1;
		maxDeals.push({
			x: tmpArr[idx].x,
			y: tmpArr[idx].y,
			buySell: null,
			sumMoney: 1000
		});
		maxDeals.push({
			x: tmpArr[idx1].x,
			y: tmpArr[idx1].y,
			buySell: -1000 * (1 - rate / 100) / tmpArr[idx].y,
			sumMoney: null
		});
	}

	return maxDeals;

}


function minDealRecd(dataArr, begEndDates, rate) {
	//获取收益最小的交易
	let tmpArr = getDataArr(dataArr, begEndDates);
	let minDeals = [];
	let minOffset = [];
	tmpArr.forEach((item, index) => {
		let minIdx = null;
		if (index + 1 < tmpArr.length) {
			minIdx = getMinDataIdx(tmpArr.slice(index)) + index;
		} else {
			//当前为最后一个净值
			minIdx = index;
		}
		minOffset.push({
			offset: tmpArr[minIdx].y - item.y,
			idx: index,
			idx1: minIdx
		});
	})

	console.log('净值数组计数', tmpArr.length, '最大亏损数组计数', minOffset.length);
	let topOffset =
		minOffset.sort((item0, item1) => {
			return item0.offset - item1.offset;
		});
	console.log('排序后的交易结果', topOffset);

	if (topOffset && topOffset.length > 0) {
		let tmp = topOffset[0];
		let idx = tmp.idx;
		let idx1 = tmp.idx1;
		minDeals.push({
			x: tmpArr[idx].x,
			y: tmpArr[idx].y,
			buySell: null,
			sumMoney: 1000
		});
		minDeals.push({
			x: tmpArr[idx1].x,
			y: tmpArr[idx1].y,
			buySell: -1000 * (1 - rate / 100) / tmpArr[idx].y,
			sumMoney: null
		});
	}

	return minDeals;
}



//dataArr,begEndDates,weekMonth,dayNum){
function monthDayDealRecd(dataArr, begEndDates, weekMonth, dayNum) {
	//每月的哪天，进行定投，weekMonth-0
	//每周的哪天，进行定投，weekMonth-1
	let tmpArr = getDataArr(dataArr, begEndDates);
	let monDeals = [];
	let lstMonth = -1;
	// let lstIndex = -2;
	let bCurFnd = false;
	let bNxtFnd = false;
	let curYearMon = null;
	let weekI = 0;

	// console.log('begEndDates,weekMonth,dayNum',begEndDates,weekMonth,dayNum)
	tmpArr.forEach((item, index) => {
		let dateX = new Date(item.x);
		//按月定投
		if (weekMonth == 0) {

			curYearMon = dateX.getYear() * 100 + dateX.getMonth();
			if (lstMonth == -1) {
				lstMonth = curYearMon;
			}


			//bCurFnd = lstMonth == dateX.getMonth();
			//同一个月
			if (lstMonth == curYearMon && dayNum <= dateX.getDate()) {
				//同一个月，交易日期后移
				bCurFnd = true;
				if ((dateX.getMonth() + 1) % 12 == 0)
					lstMonth = dateX.getYear() * 100 + 100
				else
					lstMonth = curYearMon + 1;
			} else if (lstMonth < curYearMon) {
				//当月不存在指定交易日，交易日后移到下一个月
				bNxtFnd = lstMonth < curYearMon;
				lstMonth = curYearMon;
			}

			if (bCurFnd || (!bCurFnd && bNxtFnd)) {
				monDeals.push({
					x: item.x,
					y: item.y,
					buySell: null,
					sumMoney: 100
				});

				bCurFnd = false;
				bNxtFnd = false;
			}

		}
		//按周定投
		else if (weekMonth == 1) {
			//是否同一周已经找到指定的周几
			if (dayNum == dateX.getDay()) {
				monDeals.push({
					x: item.x,
					y: item.y,
					buySell: null, //100 / item.y,
					sumMoney: 100
				});

			}
		}
		//按两周定投
		else if (weekMonth == 2) {
			//是否同一周已经找到指定的周几
			if (dayNum == dateX.getDay()) {
				if (weekI + 1 >= weekMonth) {
					monDeals.push({
						x: item.x,
						y: item.y,
						buySell: null, //100 / item.y,
						sumMoney: 100
					});
					weekI = 0;
				} else
					weekI = weekI + 1;
			}
		}
		//每天定投
		else if (weekMonth == 3) {
			monDeals.push({
				x: item.x,
				y: item.y,
				buySell: null, //100 / item.y,
				sumMoney: 100
			});

		}
	});

	return monDeals;
}
