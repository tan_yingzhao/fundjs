

//导入express框架
const express = require("express");
//导入axios插件
const axios =require("axios");
//初始化express
const app = express();

//本地服务器解决跨域，不可缺
app.all('*', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    //Access-Control-Allow-Headers ,可根据浏览器的F12查看,把对应的粘贴在这里就行
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Access-Control-Allow-Methods', '*');
    res.header('Content-Type', 'application/json;charset=utf-8');
    next();
  });

//get接口访问，访问自己这个服务器接口
app.get("/fund",function(req,res){
    //服务器获取数据，将不会产生跨域问题
    console.log('params:::', req.query);
	
	//获取基金号码
	const fundCode = req.query['fundCode'];
    
	let url = `http://fund.eastmoney.com/pingzhongdata/${fundCode}.js?v=20160518155842`;
	console.log('fundCode,url:::',fundCode,url);
    axios.get(url,
    {
        // params:{
        //     'year-month':yearmon,
        //     'key':key
        // }
    })
    .then((response) => {
        //以json格式将服务器获取到的数据返回给前端。
        res.json(response.data);
    })
    .catch(err => {
        console.log('error',err);
    })
})
//启动server，端口3000
var server = app.listen(9898,function(){
    console.log("开启成功！");
})