unit staff_post_sala_Frm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, ComCtrls, ExtCtrls, RzPanel, RzSplit, DB,
  DBAccess, Ora, VirtualTable, MemDS,U_publicFun, RzTabs;

type
  Tstaff_sala_post_Form = class(TForm)
    RzSizePanel3: TRzSizePanel;
    trView_PostItem: TTreeView;
    m_Query: TOraQuery;
    vt_post_sala: TVirtualTable;
    ods_post_sala: TOraDataSource;
    RzPageControl1: TRzPageControl;
    TabSheet1: TRzTabSheet;
    TabSheet2: TRzTabSheet;
    TabSheet3: TRzTabSheet;
    DBGridEh5: TDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure trView_PostItemClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function initInfo():Boolean;
  end;

var
  staff_sala_post_Form: Tstaff_sala_post_Form;

implementation

uses mainfrm_u;

{$R *.dfm}

procedure Tstaff_sala_post_Form.FormCreate(Sender: TObject);
begin
   initInfo();
end;

function Tstaff_sala_post_Form.initInfo():Boolean;
var
  sql:string;
  topLevel:Integer;
  topNode:TTreeNode;
  tv:TTreeView;
begin
  //初始化左侧按量计酬树控件
  sql := ' select   level ilev   , node, parnode ,item from tbxg_dict_post_sala_item '
        +' connect by prior node = parnode start with parnode is null  ';
  mainfrm.myquery(m_Query,sql);
  if(not m_Query.IsEmpty)then
  begin

//    topNode := nil;
//    tmpNode := nil;
//    preLevel := -1;
//    curLevel := -1;
//    bHasImage := (tv.Images <> nil) and (tv.Images.Count >= 1);
    tv := trView_PostItem;
    if(not m_Query.IsEmpty)then
    begin
      topLevel := m_Query.FieldByName('ilev').AsInteger;
      topNode := buildTree( tv,tv.Items.GetFirstNode(),m_Query ,topLevel,'ilev','node','item' );
    end;

  end;

  
  topNode := tv.Items.GetFirstNode();
  //全部展开,并且默认选中添加的首个
  if(tv.Items.Count > 0) and (topNode <> nil) then
  begin
     topNode.Expand(True);
//     if(grpIds <> '') then
//        selTreeNodeEx( tv , '-' + grpIds )
//     else
        tv.Selected := topNode;
  end;

end;

procedure Tstaff_sala_post_Form.trView_PostItemClick(Sender: TObject);
begin
   //获取当前选中的节点
   //控制不同的表格显示
   //之后进行查询
   



   //其次在父窗体更新底下状态栏信息，
   //获取其成本统计
   

end;


//补录1-6月份数据
//通过按月导出当月BC账发放的数据
//对应绩效工资部分的总和
//关键是字段如何对应
//在原表格上加入隐藏列的字段名称即可

select sal.年月， sal.职工ID  , sum(应发合计)  from 
(select 年月,职工ID,项目名称,应发合计,人员类别 from tb_jjsalary
union all 
 select 年月,职工ID,项目名称,应发合计,人员类别 from tb_lwgjjsalary ) sal
where exists (select * from tb_salarys s where 金额类别 = 4 and s.类型 = sal.项目名称 and 人员类别 = sal.人员类别) 
grouop by sal.年月， sal.职工ID 

--加入按量计酬的所有字段，或者是
join (select * from tb_empmonthinfo) i on i.职工ID = a.职工ID and i.年月 = a.年月 and i.new_belong_id in (2,3)



end.
